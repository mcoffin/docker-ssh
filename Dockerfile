FROM alpine:latest

RUN apk update && apk upgrade
RUN apk add openssh

ENTRYPOINT ["/usr/bin/ssh"]
